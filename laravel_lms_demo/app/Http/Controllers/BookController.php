<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $book = Book::latest()->paginate(5);

        return view('book',compact('book'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $author =Author::all();

        return view('bookcreate',compact('author'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $book
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'author_id' => 'required',
            'title' => 'required|min:3',
            'pages' => 'required',
            'langauge' => 'required',
            'cover_image' => 'required',
            'isbn' => 'required',
            'description' => 'required',

        ]);

        $image = $request->file('cover_image');
        $originalname = $image->getClientOriginalName();
        $path = $image->storeAs('public',$originalname);





//        $file =$request->file('cover_image');
//        $originalname = $request['isbn'].'.'.$file->getClientOriginalExtension() ;
//        $filename= $file->storeAs('images',$request['title']);



        Book::create($request->all());
        return redirect()->route('book.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {

        return view('showbook',compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {

        $author =Author::all();
        return view('bookedit',compact('book','author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $request->validate([
            'author_id' => 'required',
            'title' => 'required|min:3',
            'pages' => 'required',
            'langauge' => 'required',

            'isbn' => 'required|min:10|max:13',
            'description' => 'required',

        ]);

        $book->update($request->all());
        $image = $request->file('cover_image');
        $originalname = $image->getClientOriginalName();
        $path = $image->storeAs('public',$originalname);

        return redirect()->route('book.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect()->route('book.index');
    }
    public function Confirm_(Request $request)
    {
        $id = $request->input('v');
        $status=$request->input('status');


        Book::
        where('id',$id)
            ->update(['status'=>$status]);

        return redirect()->route('book.index');

    }
//    public function updateBookStatus($id){
//        $book=Book::find($id);
//        if ($book->status==1){
//            $book->status=0;
//            $book->save();
//            return redirect()->route('book.index');
//        }else{
//            $book->status=1;
//            $book->save();
//            return redirect()->route('book.index');
//        }
//    }
}
