<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $author = Author::latest()->paginate(5);

        return view('author',compact('author'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
        //return view('author');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('authorcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'fullname' => 'required|min:6|max:30',
            'dob' => 'required|date',
            'gender' => 'required|in:male,female',
            'address_' => 'required',
            'mobile_no' => 'required|min:9',
            'description_' => 'required',

        ]);
        Author::create($request->all());

        return redirect()->route('author.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        return view('showauthor',compact('author'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit(Author $author)
    {
        return view('authoredit',compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Author $author)
    {

        $request->validate([
            'fullname' => 'required|min:6|max:30',
            'dob' => 'required|date',
            'gender' => 'required|in:male,female',
            'address_' => 'required',
            'mobile_no' => 'required',
            'description_' => 'required',

        ]);

        $author->update($request->all());

        return redirect()->route('author.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy(Author $author)
    {
                $author->delete();
        return redirect()->route('author.index');
    }

    public function Confirm(Request $request)
    {
        $id = $request->input('v');
        $status=$request->input('status');


        Author::
            where('id',$id)
            ->update(['status'=>$status]);

        return redirect()->route('author.index');
    }
//    public function updateauthorStatus($id){
//        $Author=Author::find($id);
//        if ($Author->status==1){
//            $Author->status=0;
//            $Author->save();
//            return redirect()->route('author.index');
//        }else{
//            $Author->status=1;
//            $Author->save();
//            return redirect()->route('author.index');
//        }
//    }
}
