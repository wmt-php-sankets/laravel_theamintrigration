<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = [
        'fullname', 'dob', 'gender','address_','mobile_no','description_'
    ];

    public function  books(){
        return $this->hasMany('App\Book');
    }
}
