<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/ajex', function () {
    return view('ajexlaravel/profile');
});


//Route::view('ajex','profile');
//Route::get('/ajex', function () {
//    return view('author');
//});


Route::resource('/author','AuthorController');
Route::resource('/book','BookController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/active-deactive','AuthorController@Confirm');
Route::post('/active-deactive1','BookController@Confirm_');
//Route::get('/updateBookStatus/{id}','BookController@updateBookStatus');
