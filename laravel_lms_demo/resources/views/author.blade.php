@extends('layouts.app')
@section('js')
    <script src="{{asset('assets/js/plugins/table/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/plugins/forms/datatables/selects/select2.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('assets/js/demo_pages/datatables_advanced.js')}}" type="text/javascript"></script>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card  bg-primary">
                    <h3 class="mx-auto my-auto text-white text-uppercase">Author Data</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-title">
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <button class="btn btn-primary "><a href="{{ route('author.create') }}" class="text-white">Fill
                                    Author Data</a></button>
                        </div>

                        <table class="table bg-primary datatable-dom-position">
                            <thead>
                            <tr>
                                <th>Full Name</th>
                                <th>Dob</th>
                                <th>Gender</th>
                                <th>Address</th>
                                <th>Mobile_no</th>
                                <th class="text-center">Description</th>
                                {{--                            <th class="text-center">Status</th>--}}
                                <th scope="col">Status</th>

                                <th scope="col">
                                    Activity
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($author as $as)
                                <tr>
                                    <th scope="row">{{$as->fullname}}</th>
                                    <td>{{$as->dob}}</td>
                                    <td>{{$as->gender}}</td>
                                    <td>{{$as->address_ }}</td>
                                    <td>{{$as->mobile_no}}</td>
                                    <td>{{$as->description_}}</td>
                                    <td>{{$as->status}}</td>
                                    <td><a class="btn btn-success" href="{{ route('author.edit',$as->id) }}"><span class="text-uppercase">Edit</span></a>
                                    </td>
                                    <td>
                                        <form action="{{ route('author.destroy',$as->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger text-uppercase ">Delete</button>
                                        </form>
                                    </td>
                                    <td>
                                        <button class="btn btn-dark text-uppercase"><a
                                                href="{{route('author.show',$as->id)}}"><span class="text-white">Show</span></a></button>
                                    </td>
                                    <td>
                                        <form action="/active-deactive" method="POST">
                                            @csrf
                                            <div class="input-group">
                                                <input type="hidden" class="form-control" name="v" value="{{$as->id}}">
                                                <select required class="form-control" id="status" name="status">
                                                    <option value="1">Active</option>
                                                    <option value="0">De-Active</option>
                                                </select>
                                            </div>
                                            <button type="submit" class="btn btn-primary">
                                                <span>Confirm</span>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
