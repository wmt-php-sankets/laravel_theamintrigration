@extends('layouts.app')
@section('content')
    <table class="table table-striped table-hover table-dark">
        <thead>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-title">
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <button class="btn btn-primary "><a href="{{ route('book.create') }}" class="text-white text-uppercase">Fill book Data</a></button>
                        </div>
                            <table class="table bg-primary datatable-dom-position">
                            <thead>
                            <tr class="text-uppercase text-dark">
                                <th scope="col">Title</th>
                                <th scope="col">pages</th>
                                <th scope="col">langauge</th>
                                <th scope="col">cover_image</th>
                                <th scope="col">isbn</th>
                                <th scope="col">description</th>
                                <th scope="col">status</th>
                                <th scope="col">Created At</th>
                                <th scope="col">Updated At</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{$book->title}}</td>
                                <td>{{$book->pages}}</td>
                                <td>{{$book->langauge }}</td>
                                <td><img src="{{$book->cover_image  }}" height="100px" width="100px"></td>
                                <td>{{$book->isbn}}</td>
                                <td>{{$book->description}}</td>
                                <td>{{$book->status}}</td>
                                <td>{{$book->created_at}}</td>
                                <td>{{$book->updated_at}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
