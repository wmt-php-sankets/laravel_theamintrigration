@extends('layouts.app')
@section('js')
    <script src="{{url('assets/js/plugins/forms/styling/uniform.min.js')}}" type="text/javascript"></script>
    {{--        <script src="{{ url('js/app.js') }}"></script>--}}
    <script src="{{url('assets/js/plugins/forms/inputs/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/js/plugins/forms/inputs/maxlength.min.js')}}" type="text/javascript"></script>
{{--    <script src="{{url('assets/js/demo_pages/form_inputs.js')}}" type="text/javascript"></script>--}}
{{--    <script src="{{url('assets/js/plugins/forms/inputs/autosize.min.js')}}" type="text/javascript"></script>--}}
{{--    <script src="{{url('assets/js/plugins/forms/inputs/formatter.min.js')}}" type="text/javascript"></script>--}}
{{--    <script src="{{url('assets/js/plugins/forms/inputs/passy.js')}}" type="text/javascript"></script>--}}
{{--    <script src="{{url('assets/js/plugins/forms/inputs/typeahead/handlebars.min.js')}}" type="text/javascript"></script>--}}
{{--    <script src="{{url('assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}" type="text/javascript"></script>--}}

@endsection
@section('content')
    <div class="container">

        <form action="{{ route('book.store') }}" method="POST" id="myform" name="form" enctype="multipart/form-data">
            @csrf

            <div class="form-group row">
                <label class="col-form-label col-lg-2">Book Author</label>
                <div class="col-lg-10">
                    <select class="custom-select bg-primary @error('book_author') is-invalid @enderror" name="author_id"
                            id="author_id">
                        <option hidden value="">Book Author</option>
                        @foreach($author as $author)
                            <option value="{{ $author->id }}">{{ $author->fullname}}</option>
                        @endforeach
                    </select>
                    <span class="text-danger">
                @error('author_id')
                        {{$message}}
                        @enderror
                </span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-2">Title</label>
                <div class="col-lg-10">
                    <input type="text" name="title" id="title" class="form-control bg-primary" aria-describedby="emailHelp"
                           placeholder="Enter Book Title">
                    <span class="text-danger">
                @error('title')
                        {{$message}}
                        @enderror
                </span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-md-2">Pages</label>
                <div class="col-md-10">
                    <input type="number" name="pages" id="pages" class="form-control bg-primary" id="lastname"
                           aria-describedby="emailHelp" placeholder="Enter Pages">
                    <span class="text-danger">
                        @error('pages')
                        {{$message}}
                        @enderror
                </span>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-form-label col-lg-2">langauge</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control bg-primary" name="langauge" id="langauge" placeholder="Enter Book Langauge">
                    <span class="text-danger">
                @error('langauge')
                        {{$message}}
                        @enderror
                </span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-2">Cover Image</label>
                <div class="col-lg-10">
                    <input type="file" class="form-control-uniform-custom" id="image" name="cover_image" placeholder="Enter Cover Image">
                    <span class="text-danger">
                @error('cover_image')
                        {{$message}}
                        @enderror
                </span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-2">ISBN Number</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control bg-primary" name="isbn" id="isbn"  data-mask="999-99-999-9999-9" placeholder="Isbn Number">
                    <span class="form-text text-muted">999-99-999-9999-9</span>
                    <span class="text-danger">@error('isbn'){{$message}}@enderror</span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-2">Description</label>
                <div class="col-lg-10">
                  <textarea rows="4" cols="50" id="desc" name="description" placeholder="Description">
                 </textarea>
                    <span class="text-danger">@error('description'){{$message}}@enderror</span>

                </div>
            </div>
            <button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>

        </form>
    </div>
@endsection






















