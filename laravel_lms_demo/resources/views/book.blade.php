@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card  bg-primary">
                    <h3 class="mx-auto my-auto text-white text-uppercase">Book Data</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-title">
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <button class="btn btn-primary "><a href="{{ route('book.create') }}" class="text-white text-uppercase">Fill book Data</a></button>
                        </div>
                        <table class="table bg-primary datatable-dom-position">
                            <thead>
                            <tr class="text-dark text-uppercase">
                                <th>Author Id</th>
                                <th>Title</th>
                                <th>Pages</th>
                                <th>Langauge</th>
                                <th>Cover_image</th>
                                <th class="text-center">Isbn</th>
                                <th scope="col">Status</th>
                                <th scope="col">Activity</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($book as $bk)
                                <tr>
                                    <th scope="row">{{$bk->author_id}}</th>
                                    <th scope="row">{{$bk->title}}</th>
                                    <td>{{$bk->pages}}</td>
                                    <td>{{$bk->langauge}}</td>
                                    <td>{{$bk->cover_image }}</td>
                                    <td>{{$bk->isbn}}</td>
                                    <td>{{$bk->status}}</td>
                                    <td><a class="btn btn-success" href="{{ route('book.edit',$bk->id)}}"><span class="text-white text-uppercase">Edit</span></a></td>
                                    <td>
                                        <form action="{{ route('book.destroy',$bk->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger text-uppercase text-white">Delete</button>
                                        </form>

                                    </td>
                                    <td><button class="btn btn-dark text-uppercase  "><a href="{{route('book.show',$bk->id)}}"><span class="text-white">Show</span></a></button></td>

                                    <td>

                                        <form action="/active-deactive1" method="POST">
                                            @csrf
                                            <div class="input-group">
                                                <input type="hidden" class="form-control" name="v" value="{{$bk->id}}">
                                                <select required class="form-control" id="status"  name="status">
                                                    <option value="1" class="">Active</option>
                                                    <option value="0">De-Active</option>
                                                </select>
                                            </div>
                                            <button type="submit" class="btn btn-outline-secondary">
                                                <span class="text-white">Confirm</span>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
