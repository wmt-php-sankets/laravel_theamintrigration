@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="col-md-12">
        <div class="card">
            <div class="card-title">
            </div>
            <div class="card-body">
                <div class="card">
                    <div class="card-header header-elements-inline">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>

                    <table class="table bg-primary datatable-dom-position">
                        <thead>
                        <tr class="text-uppercase text-dark">
                            <th>Full Name</th>
                            <th>Dob</th>
                            <th>Gender</th>
                            <th>Address</th>
                            <th>Mobile_no</th>
                            <th class="text-center">Description</th>
                            <th scope="col">Status</th>
                            <th class="text-center">Created_at</th>
                            <th class="text-center">Updated_at</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">{{$author->fullname}}</th>
                            <td>{{$author->dob}}</td>
                            <td>{{$author->gender}}</td>
                            <td>{{$author->address_ }}</td>
                            <td>{{$author->mobile_no}}</td>
                            <td>{{$author->description_}}</td>
                            <td>{{$author->status}}</td>
                            <td>{{$author->created_at}}</td>
                            <td>{{$author->updated_at}}</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
