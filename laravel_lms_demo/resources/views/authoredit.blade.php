@extends('layouts.app')
@section('js')
    <script src="{{asset('assets/js/plugins/radio/checkboxes_radios.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/js/plugins/forms/styling/uniform.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/js/plugins/forms/styling/switchery.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/js/plugins/forms/styling/switch.min.js')}}" type="text/javascript"></script>
    {{--    <script src="{{ url('js/app.js') }}"></script>--}}

    <script src="{{url('assets/js/plugins/forms/inputs/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/js/plugins/forms/inputs/maxlength.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/js/demo_pages/form_inputs.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/js/plugins/forms/inputs/formatter.min.js')}}" type="text/javascript"></script>
    <script src="{{url('assets/js/demo_pages/form_controls_extended.js')}}" type="text/javascript"></script>
@endsection
@section('content')
    <div class="container">

        <form action="{{ route('author.update',$author->id) }}" method="POST" id="myform" name="form">

            @method('PUT')
            @csrf

            <div class="form-group row">
                <label class="col-form-label col-lg-2">Full name</label>
                <div class="col-lg-10">
                    <input type="text" name="fullname" id="firstname"
                           class="form-control aa  text-violet  bg-primary border-info-600 border-1"  value="{{$author->fullname}}" id="firstname"
                           aria-describedby="emailHelp" >
                    <span class="text-danger">  @error('fullname')
                        {{$message}}
                        @enderror
                    </span>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-form-label col-md-2">Date</label>
                <div class="col-md-10">
                    <input type="date" class="form-control bg-primary border-info-600 border-1" name="dob" id="dob"
                           value="{{$author->dob}}">
                    <span class="text-danger">  @error('dob')
                        {{$message}}
                        @enderror
                </span>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-form-label col-md-2">Select Gender</label>
                <div class="col-md-10">
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input-styled" name="gender" value="male" checked
                                   data-fouc>
                            Male
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input-styled" name="gender" value="female" data-fouc>
                            Female
                        </label>
                    </div>
                    <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input-styled" name="gender" value="other" data-fouc>
                            Other
                        </label>
                    </div>
                    <span class="text-danger">  @error('gender')
                        {{$message}}
                        @enderror
                </span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-md-2">Address</label>
                <div class="col-md-10">
                    <input type="text" class="form-control bg-primary border-info-600 border-1" value="{{$author->address_}}" id="desc1"
                           name="address_" placeholder="Enter your Address">
                    <span class="text-danger">  @error('address_')
                        {{$message}}
                        @enderror
                </span>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-md-2 ">Mobile Number</label>
                <div class="col-md-10">
                    <input type="text"  value="{{$author->mobile_no}}" class="form-control format-phone-number bg-primary border-info-600 border-1" id="m_number"
                           name="mobile_no"
                           placeholder="Enter Mobile Number">
                    <span class="text-danger">  @error('mobile_no')
                        {{$message}}
                        @enderror
                </span>
                </div>
            </div>



            <div class="form-group row">
                <label class="col-form-label col-lg-2">Description</label>
                <div class="col-lg-10">
                    <textarea rows="4" cols="50" id="desc" name="description_">
                            {{$author->description_}}
                    </textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2"></label>
                <div class="col-lg-10">
                    <button type="submit" class="btn btn-primary" name="submit" id="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
